patch('CMakeLists.txt')
patch('Alc/alcConfig.c')
patch_tree('base_dir_config.patch')

build_cmake(options='-DEXAMPLES=OFF')

collect_libraries('OpenAL32')
collect_programs('openal-info')
collect_licenses('COPYING README')
collect_file('alsoftrc.sample', 'conf/example-alsoft.ini')
