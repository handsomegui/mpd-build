# Patches from archlinux
# https://projects.archlinux.org/svntogit/packages.git/tree/trunk?h=packages/libid3tag

patch_tree('CVE-2008-2109.patch', 0)
patch_tree('unknown_encoding.patch')
patch_tree('utf16.patch')

build(static_lib=True)
collect_licenses('COPYRIGHT COPYING CREDITS')
