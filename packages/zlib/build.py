import cmdutil

install_dir = cmdutil.to_unix_path(current_package.install_dir)

make_args = ('install -fwin32/Makefile.gcc SHARED_MODE=0' +
     ' INCLUDE_PATH=' + install_dir + '/include' +
     ' BINARY_PATH='  + install_dir + '/bin' +
     ' LIBRARY_PATH=' + install_dir + '/lib' +
     ' prefix=' + install_dir)

if current_project.crossbuild:
    make_args += ' PREFIX=' + current_project.host_triplet + '-'

unix_make(make_args)
collect_licenses('README')
